from aiogram.fsm.state import StatesGroup, State


class UserInput(StatesGroup):
    reels = State()
    hashtag = State()
    gpt = State()


class AdminInput(StatesGroup):
    spam = State()
    spam_image = State()
