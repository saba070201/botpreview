from datetime import datetime

from sqlalchemy import func, BigInteger, ForeignKey
from sqlalchemy.orm import mapped_column, Mapped, relationship

from app.enums.chat_type import ActionType
from app.models import dto
from app.models.db import User
from app.models.db.base import Base


class Action(Base):
    __tablename__ = "action"
    __mapper_args__ = {"eager_defaults": True}
    id: Mapped[int] = mapped_column(primary_key=True)
    user_id: Mapped[int] = mapped_column(BigInteger, ForeignKey("users.id"))
    user: Mapped[User] = relationship(foreign_keys=[user_id])
    type: Mapped[ActionType]
    date: Mapped[datetime] = mapped_column(server_default=func.now())

    def __repr__(self):
        return f"<Action ID: {self.id}, User ID: {self.user_id}, Type: {self.type}, Date: {self.date}>"

    def to_dto(self) -> dto.Action:
        return dto.Action(
            id=self.id,
            user_id=self.user_id,
            user=self.user.to_dto(),
            type=self.type,
            date=self.date
        )
