from datetime import datetime
from typing import Optional

from sqlalchemy import func, BigInteger
from sqlalchemy.orm import mapped_column, Mapped

from app.models import dto
from app.models.db.base import Base


class User(Base):
    __tablename__ = "users"
    __mapper_args__ = {"eager_defaults": True}
    id: Mapped[int] = mapped_column(BigInteger, primary_key=True)
    username: Mapped[Optional[str]]
    fullname: Mapped[str]
    registration_time: Mapped[datetime] = mapped_column(server_default=func.now())
    last_seen: Mapped[datetime] = mapped_column(default=datetime.now,
                                                onupdate=datetime.now,
                                                server_default=func.now())
    last_function: Mapped[datetime] = mapped_column(server_default=func.now())
    is_subscriber: Mapped[bool] = mapped_column(server_default='t')
    function_count: Mapped[int] = mapped_column(server_default='0')
    referral_link: Mapped[Optional[str]]

    def __repr__(self):
        return f"User [ID: {self.id}, Username: {self.username}, Fullname:{self.fullname}, " \
               f"Registration Time: {self.registration_time}, Last Seen: {self.last_seen}, " \
               f"Is Subscriber: {self.is_subscriber}, Function count: {self.function_count}]"

    def to_dto(self) -> dto.User:
        return dto.User(
            id=self.id,
            username=self.username,
            fullname=self.fullname,
            registration_time=self.registration_time,
            last_seen=self.last_seen,
            is_subscriber=self.is_subscriber,
            function_count=self.function_count,
            last_function=self.last_function,
            referral_link=self.referral_link
        )
