from __future__ import annotations

from datetime import datetime
from dataclasses import dataclass

from aiogram import types as tg


@dataclass
class User:
    id: int | None = None
    username: str | None = None
    fullname: str | None = None
    registration_time: datetime | None = None
    last_seen: datetime | None = None
    is_subscriber: bool | None = None
    function_count: int | None = None
    last_function: datetime | None = None
    referral_link: str | None = None

    @classmethod
    def from_aiogram(cls, user: tg.User) -> User:
        return cls(
            id=user.id,
            username=user.username,
            fullname=user.full_name,
            last_seen=datetime.now(),
        )
