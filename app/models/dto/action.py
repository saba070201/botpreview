from __future__ import annotations

from datetime import datetime
from dataclasses import dataclass

from aiogram import types as tg

from app.enums.chat_type import ActionType
from app.models.dto import User


@dataclass
class Action:
    id: int
    user_id: int
    user: User
    type: ActionType
    date: datetime

