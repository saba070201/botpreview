from __future__ import annotations

from dataclasses import dataclass
from pathlib import Path

from app.models.config.db import DBConfig


@dataclass
class Config:
    paths: Paths
    db: DBConfig
    bot: BotConfig
    answers: AnswersConfig

    @property
    def app_dir(self) -> Path:
        return self.paths.app_dir

    @property
    def config_path(self) -> Path:
        return self.paths.config_path


@dataclass
class Paths:
    app_dir: Path

    @property
    def config_path(self) -> Path:
        return self.app_dir / "config"

    @property
    def log_path(self) -> Path:
        return self.app_dir / "log"


@dataclass
class BotConfig:
    token: str
    superusers: list[int]
    use_redis: bool
    channel_username: str
    channel_url :str
    channel_id: int
    openai_api_key: list[str]
    rate_limit: int
    daily_limit: int


@dataclass
class AnswersConfig:
    user: UserAnswer
    admin: AdminAnswer


@dataclass
class UserAnswer:
    start: str
    reels: str
    hashtag: str
    gpt: str
    not_in_channel: str
    rate_limit: str
    wait_download: str
    wrong_url: str
    daily_limit: str
    hashtag_not_found: str
    hashtags_found: str
    wait_gpt: str
    unavailable_gpt: str
    permission_error_gpt: str
    permission_error_gpt_admin: str
    pre_add: str
    pre_add_url :str
    add: str


@dataclass
class AdminAnswer:
    admin: str
    stats_wait: str
    stats: str
    stats_file_wait: str
    spam: str
    spam_check: str
    spam_beginning: str
    spam_end: str
