from aiogram import Dispatcher
from sqlalchemy.ext.asyncio import async_sessionmaker, AsyncSession

from app.middlewares.config_middleware import ConfigMiddleware
from app.middlewares.data_load_middleware import LoadDataMiddleware
from app.middlewares.db_middleware import DBMiddleware
from app.middlewares.loggin import LoggingMiddleware
from app.middlewares.throttling import ThrottlingMiddleware
from app.models.config.main import Config


def setup_middlewares(dp: Dispatcher, pool: async_sessionmaker[AsyncSession], config: Config):
    for tmp in [dp.message, dp.callback_query]:
        tmp.middleware(LoggingMiddleware())
        tmp.middleware(ConfigMiddleware(config))
        tmp.middleware(DBMiddleware(pool))
        tmp.middleware(LoadDataMiddleware())
