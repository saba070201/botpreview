import logging
from typing import Callable, Any, Awaitable

from aiogram import BaseMiddleware
from aiogram.types import TelegramObject, Message, CallbackQuery
from sqlalchemy.ext.asyncio import async_sessionmaker, AsyncSession

logger = logging.getLogger(__name__)


class LoggingMiddleware(BaseMiddleware):

    async def __call__(
            self,
            handler: Callable[[TelegramObject, dict[str, Any]], Awaitable[Any]],
            event: TelegramObject,
            data: dict[str, Any]
    ) -> Any:
        # logger.info(event)
        # logger.info(type(event))
        if type(event) == Message:
            event: Message
            try:
                logger.info(f"Received message from User[ID:{event.from_user.id}] | {event.text} ")
            except UnicodeEncodeError:
                logger.info(f"Failed to log message from User[ID:{event.from_user.id}] | UnicodeEncodeError ")
        elif type(event) == CallbackQuery:
            event: CallbackQuery
            if event.message:
                logger.info(f"Received callback query "
                            f"from User[ID:{event.from_user.id}] "
                            f"with data: {event.data}")

            else:
                logger.info(f"Received callback query [ID:{event.id}] "
                            f"from user [ID:{event.from_user.id}] "
                            f"for inline message [ID:{event.inline_message_id}] ")
        else:
            logger.info(f"Received unknown event\n{event}")
        await handler(event, data)
