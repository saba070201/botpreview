import asyncio
from typing import Callable, Any, Awaitable

from aiogram import Dispatcher, types, BaseMiddleware
from aiogram.types import TelegramObject


def rate_limit(limit: int, key=None):
    """
    Decorator for configuring rate limit and key in different functions.

    :param limit:
    :param key:
    :return:
    """

    def decorator(func):
        setattr(func, 'throttling_rate_limit', limit)
        if key:
            setattr(func, 'throttling_key', key)
        return func

    return decorator


class ThrottlingMiddleware(BaseMiddleware):
    """
    Simple middleware
    """

    def __init__(self, limit=0.4, key_prefix='antiflood_'):
        self.rate_limit = limit
        self.prefix = key_prefix
        super(ThrottlingMiddleware, self).__init__()

    async def __call__(
            self,
            handler: Callable[[TelegramObject, dict[str, Any]], Awaitable[Any]],
            event: TelegramObject,
            data: dict[str, Any]
    ) -> Any:
        if handler:
            limit = getattr(handler, 'throttling_rate_limit', self.rate_limit)
            key = getattr(handler, 'throttling_key', f"{self.prefix}_{handler.__name__}")
        else:
            limit = self.rate_limit
            key = f"{self.prefix}_message"
        print(f'{limit}')
        print(f'{key}')
        # try:
        #     await dispatcher.throttle(key, rate=limit)
        # except Throttled as t:
        #     # Execute action
        #     await self.message_throttled(callback_query.message, Throttled)
        #
        #     # Cancel current handler
        #     raise CancelHandler()

        return await handler(event, data)

    # async def message_throttled(self, message: types.Message,
    #                             throttled: Throttled,
    #                             handler: Callable[[TelegramObject, dict[str, Any]], Awaitable[Any]]):
    #     """
    #     Notify user only on first exceed and notify about unlocking only on last exceed
    #
    #     :param message:
    #     :param throttled:
    #     """
    #     print(throttled)
    #     handler = current_handler.get()
    #     dispatcher = Dispatcher.get_current()
    #     if handler:
    #         key = getattr(handler, 'throttling_key', f"{self.prefix}_{handler.__name__}")
    #     else:
    #         key = f"{self.prefix}_message"
    #
    #     # Calculate how many time is left till the block ends
    #     delta = throttled.rate - throttled.delta
    #
    #     # Prevent flooding
    #     if throttled.exceeded_count <= 2:
    #         await message.reply('Слишком много запросов!')
    #
    #     # Sleep.
    #     await asyncio.sleep(delta)
    #
    #     # Check lock status
    #     thr = await dispatcher.check_key(key)
    #
    #     # If current message is not last with current key - do not send message
    #     if thr.exceeded_count == throttled.exceeded_count:
    #         await message.reply('Разблокировано')
