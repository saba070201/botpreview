from pathlib import Path

from aiogram.types import Message

from app.config import load_config
from app.models.config.main import Paths

config = load_config(Paths(Path().absolute()))

superusers = config.bot.superusers


async def is_superuser(message: Message) -> bool:
    return message.from_user.id in superusers
