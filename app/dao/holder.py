from dataclasses import dataclass, field

from sqlalchemy.ext.asyncio import AsyncSession

from app.dao import UserDAO
from app.dao.action import ActionDAO


@dataclass
class HolderDao:
    session: AsyncSession
    user: UserDAO = field(init=False)
    action: ActionDAO = field(init=False)

    def __post_init__(self):
        self.user = UserDAO(self.session)
        self.action = ActionDAO(self.session)

    async def commit(self):
        await self.session.commit()
