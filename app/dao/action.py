from datetime import datetime

from sqlalchemy import select, func
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.ext.asyncio import AsyncSession

from app.dao.base import BaseDAO
from app.models import dto
from app.models.db import User
from app.models.db.action import Action


class ActionDAO(BaseDAO[Action]):
    def __init__(self, session: AsyncSession):
        super().__init__(Action, session)

    async def count_user_actions(self,
                                 user_id: int,
                                 today_start: datetime) -> int:
        result = await self.session.execute(
            select(func.count(Action.id)).where(Action.date > today_start,
                                                Action.user_id == user_id)
        )
        return result.scalar_one()
