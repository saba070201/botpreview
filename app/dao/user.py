import datetime

from sqlalchemy import select, func
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.ext.asyncio import AsyncSession

from app.dao.base import BaseDAO
from app.models import dto
from app.models.db import User
from app.models.db.action import Action


class UserDAO(BaseDAO[User]):
    def __init__(self, session: AsyncSession):
        super().__init__(User, session)

    async def upsert_user(self, user: dto.User) -> dto.User:
        kwargs = dict(
            id=user.id,
            fullname=user.fullname,
            username=user.username,
            last_seen=datetime.datetime.now()
        )
        saved_user = await self.session.execute(
            insert(User)
            .values(**kwargs)
            .on_conflict_do_update(
                index_elements=(User.id,), set_=kwargs, where=User.id == user.id
            )
            .returning(User)
        )
        return saved_user.scalar_one().to_dto()

    async def increment_function_count(self, dto_user: dto.User):
        user = await self.get_by_id(dto_user.id)
        user.function_count += 1
        await self.commit()

    async def last_function(self, dto_user: dto.User):
        user = await self.get_by_id(dto_user.id)
        user.last_function = datetime.datetime.now()
        await self.commit()
        return user.to_dto()

    async def count_subscribers(self):
        result = await self.session.execute(
            select(func.count(User.id)).where(User.is_subscriber == False)
        )
        return result.scalar_one()

    async def set_referral_link(self, dto_user: dto.User, referral_link: str):
        user = await self.get_by_id(dto_user.id)
        user.referral_link = referral_link

