import logging
from datetime import timedelta, datetime

from aiogram import Bot, Router
from aiogram.filters import Command, CommandStart, CommandObject
from aiogram.fsm.context import FSMContext
from aiogram.types import Message

from app.dao.holder import HolderDao
from app.filters.superusers import is_superuser
from app.handlers.base import start_action
from app.handlers.superuser import admin_action
from app.models import dto
from app.models.config.main import UserAnswer, AdminAnswer
from app.services.user import set_referral_link

router = Router(name=__name__)
logger = logging.getLogger(__name__)


@router.message(CommandStart())
async def start_cmd(message: Message,
                    command: CommandObject,
                    state: FSMContext,
                    bot: Bot,
                    user: dto.User,
                    dao: HolderDao,
                    user_answer: UserAnswer):
    if (referrer_id := command.args) and datetime.now() - user.registration_time < timedelta(seconds=5):
        await set_referral_link(user, dao.user, referrer_id)
    await start_action(message.from_user.id, state, bot, user_answer)


@router.message(Command('admin'), is_superuser)
async def admin_cmd(message: Message, state: FSMContext, bot: Bot, admin_answer: AdminAnswer):
    await admin_action(message.from_user.id, state, bot, admin_answer)
