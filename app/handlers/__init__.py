from .command import router as command_router
from .base import router as base_router
from .superuser import router as super_router

routers = (command_router, base_router, super_router)

