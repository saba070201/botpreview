import asyncio
import datetime
import logging
from pprint import pprint
from typing import Optional

from aiogram import Router, Bot, F
from aiogram.fsm.context import FSMContext
from aiogram.types import Message, CallbackQuery, FSInputFile
from openai import error as openai_error

from app.api import gpt, instagram
from app.api.instagram import PrivateAccountException, insta_sessions
from app.dao.holder import HolderDao
from app.enums.chat_type import ActionType
from app.keyboard.user import inline
from app.models import dto
from app.models.config.main import BotConfig, UserAnswer
from app.models.states.user import UserInput
from app.services.action import count_user_actions, insert_action
from app.services.user import increment_function_count

logger = logging.getLogger(__name__)

router = Router(name=__name__)


async def start_action(user_id: int, state: FSMContext, bot: Bot, user_answer: UserAnswer):
    current_state = await state.get_state()
    if current_state:
        await state.clear()
    await bot.send_message(chat_id=user_id,
                           text=user_answer.start,
                           reply_markup=inline.menu)


@router.callback_query(F.data == 'start')
async def start_clb(callback: CallbackQuery, state: FSMContext, bot: Bot, user_answer: UserAnswer):
    await start_action(callback.from_user.id, state, bot, user_answer)


@router.callback_query(F.data == 'reels')
async def my_callback_foo(callback: CallbackQuery, state: FSMContext, user_answer: UserAnswer):
    await state.set_state(UserInput.reels)
    await callback.message.answer(user_answer.reels,
                                  reply_markup=inline.back)


@router.callback_query(F.data == 'hashtag')
async def my_callback_foo(callback: CallbackQuery, state: FSMContext, user_answer: UserAnswer):
    await state.set_state(UserInput.hashtag)
    await callback.message.answer(user_answer.hashtag,
                                  reply_markup=inline.back)


@router.callback_query(F.data == 'gpt')
async def my_callback_foo(callback: CallbackQuery, state: FSMContext, user_answer: UserAnswer):
    await state.set_state(UserInput.gpt)
    await callback.message.answer(user_answer.gpt,
                                  reply_markup=inline.back,disable_web_page_preview=True)


async def is_in_channel(user_id: int,
                        bot: Bot,
                        chat_id: Optional[int] = None,
                        channel_username: Optional[str] = None) -> bool:
    group_member = None
    if chat_id:
        group_member = await bot.get_chat_member(chat_id=chat_id, user_id=user_id)
    if channel_username:
        group_member = await bot.get_chat_member(chat_id=channel_username,
                                                 user_id=user_id)
    if group_member.status not in ("creator", "administrator", "member"):
        return False
    return True


async def check_channel(user_id: int, state: FSMContext, bot: Bot, user: dto.User, dao: HolderDao, config: BotConfig,
                        user_answer: UserAnswer):
    if not await is_in_channel(user_id=user_id,
                               bot=bot,
                               channel_username=config.channel_username):
        await bot.send_message(chat_id=user_id,
                               text=user_answer.not_in_channel.format(channel_username=f'<a href="{config.channel_url}"> {config.channel_username} </a>'),
                               reply_markup=inline.channel_check,parse_mode='HTML',disable_web_page_preview=True)
        return
    if datetime.datetime.now() - user.last_function < datetime.timedelta(seconds=config.rate_limit):
        await bot.send_message(chat_id=user_id,
                               text=user_answer.rate_limit.format(rate_limit=config.rate_limit))
        return

    if await count_user_actions(dao.action, user_id) >= config.daily_limit:
        await bot.send_message(chat_id=user_id,
                               text=user_answer.daily_limit)
        await start_action(user_id, state, bot, user_answer)
        return

    await dao.user.last_function(user)
    current_state = await state.get_state()
    data = await state.get_data()
    message = data['input']

    if current_state == UserInput.reels:
        await bot.send_message(chat_id=user_id,
                               text=user_answer.wait_download)
        try:
            url = instagram.format_url(message)
        except ValueError:
            await bot.send_message(chat_id=user_id,
                                   text=user_answer.wrong_url,
                                   reply_markup=inline.back)
            return

        await insert_action(action_dao=dao.action,
                            user_id=user_id,
                            action_type=ActionType.INSTA_MEDIA)
        while True:
            try:
                session_name = insta_sessions.popleft()
            except IndexError:
                logger.error('There is no insta sessions')
                await bot.send_message(chat_id=user_id,
                                       text='Сервис сейчас недоступен')
                return
            loader = instagram.get_instaloader(session_name)
            try:
                match url.path.split('/')[1:3]:

                    case ['s', _]:
                        url = await instagram.get_highlight_url(url, loader)
                        highlights = await instagram.get_highlight(url, loader)
                        pprint(highlights)
                        for index, tmp_dict in enumerate(highlights):
                            try:
                                if 'video_versions' in tmp_dict:
                                    text = f"<a href='{tmp_dict['video_versions'][0]['url']}'>👇</a>"
                                elif 'image_versions2' in tmp_dict:
                                    text = f"<a href='{tmp_dict['image_versions2']['candidates'][0]['url']}'>👇</a>"
                                else:
                                    continue
                                if index == len(highlights) - 1:
                                    text += f'\n\n<a href="{user_answer.pre_add_url}">{user_answer.pre_add} </a>'
                                await bot.send_message(chat_id=user_id,
                                                       text=text,
                                                       reply_markup=inline.back,
                                                       parse_mode='HTML',disable_web_page_preview=True)
                                await asyncio.sleep(0.2)
                            except Exception as e:
                                logger.error('Error insta send message')
                                logger.exception(e)
                                await bot.send_message(chat_id=user_id,
                                                       text='Ошибка при загрузке',
                                                       reply_markup=inline.back,
                                                       parse_mode='HTML')

                    case ['stories', 'highlights']:
                        highlights = await instagram.get_highlight(url, loader)
                        for index, tmp_dict in enumerate(highlights):
                            try:
                                if 'video_versions' in tmp_dict:
                                    text = f"<a href='{tmp_dict['video_versions'][0]['url']}'>👇</a>"
                                elif 'image_versions2' in tmp_dict:
                                    text = f"<a href='{tmp_dict['image_versions2']['candidates'][0]['url']}'>👇</a>"
                                else:
                                    continue
                                if index == len(highlights) - 1:
                                    text += f'\n\n<a href="{user_answer.pre_add_url}">{user_answer.pre_add} </a> '
                                await bot.send_message(chat_id=user_id,
                                                       text=text,
                                                       reply_markup=inline.back,
                                                       parse_mode='HTML')
                                await asyncio.sleep(0.2)
                            except Exception as e:
                                logger.error('Error insta send message')
                                logger.exception(e)
                                await bot.send_message(chat_id=user_id,
                                                       text='Ошибка при загрузке',
                                                       reply_markup=inline.back,
                                                       parse_mode='HTML')

                    case ['stories', _]:
                        try:
                            files_path = await instagram.get_story(url, loader)
                        except PrivateAccountException:
                            await bot.send_message(chat_id=user_id,
                                                   text='Аккаунт приватный',
                                                   reply_markup=inline.back)
                            insta_sessions.append(session_name)
                            return
                        for index, file_path in enumerate(files_path):
                            caption = user_answer.pre_add if index == len(files_path) - 1 else ''
                            await bot.send_document(chat_id=user_id,
                                                    document=FSInputFile(file_path),
                                                    caption=caption)

                    case ['reel', _] | ['p', _]:
                        files_path = await instagram.get_post(url, loader)
                        for index, file_path in enumerate(files_path):
                            caption = user_answer.pre_add if index == len(files_path) - 1 else ''
                            await bot.send_document(chat_id=user_id,
                                                    document=FSInputFile(file_path),
                                                    caption=caption)

                    case _:
                        await bot.send_message(chat_id=user_id,
                                               text=user_answer.wrong_url,
                                               reply_markup=inline.back)
                        insta_sessions.append(session_name)
                        return
            except IndexError:
                await bot.send_message(chat_id=user_id,
                                       text='Сервис сейчас недоступен')
                return
            except Exception as e:
                logger.error('Removing session')
                logger.exception(e)
            else:
                insta_sessions.append(session_name)
                break

    elif current_state == UserInput.hashtag:
        if message[0] == "#":
            message = message[1:]

        await insert_action(action_dao=dao.action,
                            user_id=user_id,
                            action_type=ActionType.INSTA_HASHTAG)
        while True:
            try:
                session_name = insta_sessions.popleft()
            except IndexError:
                await bot.send_message(chat_id=user_id,
                                       text='Сервис сейчас недоступен')
                return
            loader = instagram.get_instaloader(session_name)
            try:
                hashtags = await instagram.get_hashtag(message, loader)
            except IndexError:
                await bot.send_message(chat_id=user_id,
                                       text='Сервис сейчас недоступен')
                for admin_id in config.superusers:
                    try:
                        await bot.send_message(chat_id=admin_id,
                                               text="Сессии инстаграм закончились")
                    except Exception as e:
                        logger.exception(e)
                return
            except Exception as e:
                logger.error('Removing session')
                logger.exception(e)
            else:
                insta_sessions.append(session_name)
                break
        if not hashtags:
            await bot.send_message(chat_id=user_id,
                                   text=user_answer.hashtag_not_found.format(message))
        else:
            answer = user_answer.hashtags_found
            hashtags = sorted(hashtags, key=lambda x: x.media_count, reverse=True)
            high_hashtags = list(filter(lambda x: x.media_count > 100_000, hashtags))
            answer += "\n"
            if len(high_hashtags):
                answer += f'Высокочастотные:'
                for hashtag in high_hashtags:
                    answer += f'\n#{hashtag.name} {hashtag.search_result_subtitle}'
                answer += "\n\n"

            mid_hashtags = list(filter(lambda x: 100_000 >= x.media_count > 10_000, hashtags))
            if len(mid_hashtags):
                answer += f'Среднечастотные:'
                for hashtag in mid_hashtags:
                    answer += f'\n#{hashtag.name} {hashtag.search_result_subtitle}'
                answer += "\n\n"

            low_hashtags = list(filter(lambda x: x.media_count <= 10_000, hashtags))
            if len(low_hashtags):
                answer += f'Низкочастотные:'
                for hashtag in low_hashtags:
                    answer += f'\n#{hashtag.name} {hashtag.media_count}'
            await bot.send_message(chat_id=user_id,
                                   text=f' {answer} \n <a href="{user_answer.pre_add_url}">{user_answer.pre_add} </a>',disable_web_page_preview=True)

    elif current_state == UserInput.gpt:
        await bot.send_message(chat_id=user_id,
                               text=user_answer.wait_gpt)

        await insert_action(action_dao=dao.action,
                            user_id=user_id,
                            action_type=ActionType.CHAT_GPT)
        try:
            gpt_answer = await gpt.ask(message)
        except (openai_error.ServiceUnavailableError, openai_error.APIError):
            await bot.send_message(chat_id=user_id,
                                   text=user_answer.unavailable_gpt)
        except openai_error.PermissionError:
            for admin_id in config.superusers:
                try:
                    await bot.send_message(chat_id=admin_id,
                                           text=user_answer.permission_error_gpt_admin)
                except Exception as e:
                    logger.exception(e)
            await bot.send_message(chat_id=user_id,
                                   text=user_answer.permission_error_gpt)
        else:
            final_message = f'{gpt_answer}\n\n<a href="{user_answer.pre_add_url}">{user_answer.pre_add} </a>'
            await bot.send_message(chat_id=user_id,
                                   text=final_message, disable_web_page_preview=True)
            await increment_function_count(user, dao.user)
            if user_answer.add:
                await bot.send_message(chat_id=user_id,
                                       text=user_answer.add)
            await start_action(user_id, state, bot, user_answer)
            return

    await increment_function_count(user, dao.user)
    if user_answer.add:
        await bot.send_message(chat_id=user_id,
                               text=user_answer.add)
    await start_action(user_id, state, bot, user_answer)


@router.message(UserInput.reels)
@router.message(UserInput.hashtag)
@router.message(UserInput.gpt)
async def check_channel_message(message: Message, state: FSMContext, bot: Bot, user: dto.User, dao: HolderDao,
                                config: BotConfig, user_answer: UserAnswer):
    await state.set_data({'input': message.text})
    await check_channel(message.from_user.id, state, bot, user, dao, config, user_answer)


@router.callback_query(F.data == 'check_channel')
async def check_channel_callback(callback: CallbackQuery, state: FSMContext, bot: Bot, user: dto.User, dao: HolderDao,
                                 config: BotConfig, user_answer: UserAnswer):
    await check_channel(callback.from_user.id, state, bot, user, dao, config, user_answer)
