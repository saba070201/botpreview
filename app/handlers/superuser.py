import asyncio
import logging
from pathlib import Path

from aiogram import Bot, Router, F
from aiogram.exceptions import TelegramForbiddenError, TelegramBadRequest
from aiogram.fsm.context import FSMContext
from aiogram.types import Message, CallbackQuery, FSInputFile
from openpyxl import Workbook

from app.dao.holder import HolderDao
from app.filters.superusers import is_superuser
from app.keyboard.user import inline
from app.models.config.main import AdminAnswer
from app.models.states.user import AdminInput
from app.services.user import get_all_users, set_subscriber, count_users, count_subscribers

router = Router(name=__name__)
logger = logging.getLogger(__name__)


async def admin_action(user_id: int, state: FSMContext, bot: Bot, admin_answer: AdminAnswer):
    await state.clear()
    await bot.send_message(chat_id=user_id,
                           text=admin_answer.admin,
                           reply_markup=inline.admin_menu)


@router.callback_query(F.data == 'admin', is_superuser)
async def admin_clb(callback: CallbackQuery, state: FSMContext, bot: Bot, admin_answer: AdminAnswer):
    await admin_action(callback.from_user.id, state, bot, admin_answer)


@router.callback_query(F.data == 'stats', is_superuser)
async def admin_clb(callback: CallbackQuery, state: FSMContext, dao: HolderDao, bot: Bot, admin_answer: AdminAnswer):
    await state.clear()
    await callback.message.answer(admin_answer.stats_wait)
    users = await get_all_users(dao.user)
    logger.warning('Started users scan')
    for user in users:
        try:
            await bot.send_chat_action(chat_id=user.id,
                                       action='typing')
            if not user.is_subscriber:
                await set_subscriber(user=user,
                                     user_dao=dao.user,
                                     status=True)
        except TelegramForbiddenError:
            await set_subscriber(user=user,
                                 user_dao=dao.user,
                                 status=False)
        except TelegramBadRequest as e:
            logger.exception(e.message)
            await set_subscriber(user=user,
                                 user_dao=dao.user,
                                 status=False)
        except Exception as e:
            logger.exception(e)
            await set_subscriber(user=user,
                                 user_dao=dao.user,
                                 status=False)

    await callback.message.answer(admin_answer.stats.format(followed_count=await count_users(dao.user),
                                                            unfollowed_count=await count_subscribers(dao.user)),
                                  reply_markup=inline.stats_menu)


@router.callback_query(F.data == 'stats_dl', is_superuser)
async def admin_clb(callback: CallbackQuery, dao: HolderDao, admin_answer: AdminAnswer):
    await callback.message.answer(admin_answer.stats_file_wait)
    users = await get_all_users(dao.user)
    logger.warning('Generating file')
    wb = Workbook()
    ws = wb.active
    ws.append(['ID', 'Username', 'Ф.И.О.', "Дата регистрации", 'Последняя активность', 'Подписчик',
               'Запусков функционала', 'Реферальная ссылка'])
    for user in users:
        ws.append([user.id,
                   user.username,
                   user.fullname,
                   user.registration_time,
                   user.last_seen,
                   'Да' if user.is_subscriber else 'Нет',
                   user.function_count,
                   user.referral_link or ''
                   ])
    wb.save(f"downloads/{callback.from_user.id}.xlsx")
    path = Path(Path().absolute(), 'downloads', f'{callback.from_user.id}.xlsx')
    await callback.message.answer_document(document=FSInputFile(path),
                                           reply_markup=inline.admin_back)


@router.callback_query(F.data == 'spam', is_superuser)
async def admin_clb(callback: CallbackQuery, state: FSMContext, admin_answer: AdminAnswer):
    await state.set_state(AdminInput.spam)
    await callback.message.answer(admin_answer.spam)


@router.message(AdminInput.spam, is_superuser)
async def start_cmd(message: Message, state: FSMContext, admin_answer: AdminAnswer):
    text = message.text
    if not text:
        text = message.caption if message.caption else ''
    photo = None
    if message.photo:
        photo = message.photo[-1].file_id
    await state.set_data({'text': text,
                          'photo': photo})
    await message.answer(admin_answer.spam_check,
                         reply_markup=inline.spam_menu)
    if photo:
        await message.answer_photo(photo=photo,
                                   caption=text,
                                   reply_markup=inline.spam_menu)
        return
    await message.answer(text)


@router.callback_query(F.data == 'spam_accept', is_superuser)
async def admin_clb(callback: CallbackQuery, state: FSMContext, dao: HolderDao, bot: Bot, admin_answer: AdminAnswer):
    data = await state.get_data()
    await state.clear()
    users = await get_all_users(dao.user)
    counter = 0
    await callback.message.answer(admin_answer.spam_beginning)
    logger.warning('Started spam')
    for user in users:
        # if user.id == callback.from_user.id:
        #     continue
        try:
            if data['photo']:
                await bot.send_photo(chat_id=user.id,
                                     photo=data['photo'],
                                     caption=data['text'])
            else:
                await bot.send_message(chat_id=user.id,
                                       text=data['text'])
            counter += 1
            if not user.is_subscriber:
                await set_subscriber(user=user,
                                     user_dao=dao.user,
                                     status=True)
        except TelegramForbiddenError:
            logger.warning(f"User {user.id} banned bot")
            await set_subscriber(user=user,
                                 user_dao=dao.user,
                                 status=False)
        except Exception as e:
            logger.warning(f"failed to send message to User {user.id}")
            logger.exception(e)
            await set_subscriber(user=user,
                                 user_dao=dao.user,
                                 status=False)
        await asyncio.sleep(0.2)

    await callback.message.answer(admin_answer.spam_end.format(counter=counter,
                                                               users_amount=len(users)),
                                  reply_markup=inline.admin_back)
