import asyncio
import logging
import os
from pathlib import Path

from aiogram import Dispatcher, Bot
from aiogram.fsm.storage.memory import MemoryStorage
from aiogram.fsm.storage.redis import RedisStorage
from sqlalchemy.orm import close_all_sessions

from app.config import load_config
from app.config.logging_config import setup_logging
from app.handlers import routers
from app.middlewares import setup_middlewares
from app.models.config.main import Paths
from app.models.db import create_pool

logger = logging.getLogger(__name__)


async def main():
    paths = get_paths()
    setup_logging()
    config = load_config(paths)

    if config.bot.use_redis:
        storage = RedisStorage.from_url('redis://@localhost:6379')
    else:
        storage = MemoryStorage()

    dp = Dispatcher(storage=storage)

    setup_middlewares(dp, create_pool(config.db), config)

    for router in routers:
        dp.include_router(router)

    logger.debug("handlers configured successfully")

    bot = Bot(
        token=config.bot.token,
        parse_mode="HTML",
    )

    logger.info("started")
    try:
        await dp.start_polling(bot)
    finally:
        close_all_sessions()
        logger.info("stopped")


def get_paths() -> Paths:
    if path := os.getenv("BOT_PATH"):
        return Paths(Path(path))
    return Paths(Path(__file__).parent.parent)


def cli():
    """Wrapper for command line"""
    try:
        asyncio.run(main())
    except (KeyboardInterrupt, SystemExit):
        logger.error("Bot stopped!")
