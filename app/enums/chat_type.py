import enum


class ChatType(enum.Enum):
    private = enum.auto()
    channel = enum.auto()
    group = enum.auto()
    supergroup = enum.auto()


class ActionType(enum.Enum):
    CHAT_GPT = 'ChatGPT'
    INSTA_MEDIA = 'Insta Media'
    INSTA_HASHTAG = 'Insta Hashtag'
