import datetime
import logging.config
import os


logger = logging.getLogger(__name__)


def setup_logging():
    try:
        os.mkdir("logs")
    except FileExistsError:
        pass
    logging_handlers = [logging.StreamHandler(),
                        logging.FileHandler(f"logs/{datetime.datetime.now().strftime('%Y_%m_%d-%H_%M')}.log")]
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s.%(msecs)03d | %(name)-30s | %(levelname)-8s | #%(lineno)-5d| %(message)s \n",
        datefmt="%Y-%m-%d %H:%M:%S",
        handlers=logging_handlers
    )
