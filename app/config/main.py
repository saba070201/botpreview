import logging.config

import yaml

from app.config.db import load_db_config
from app.models.config import Config
from app.models.config.main import Paths, BotConfig, AnswersConfig, UserAnswer, AdminAnswer

logger = logging.getLogger(__name__)


def load_config(paths: Paths) -> Config:
    with (paths.config_path / "config.yaml").open("r") as f:
        config_dct = yaml.safe_load(f)
    with (paths.config_path / "bot_answers.yaml").open("r", encoding="utf8") as z:
        answers_dct = yaml.safe_load(z)

    return Config(
        paths=paths,
        db=load_db_config(config_dct["db"]),
        bot=load_bot_config(config_dct["bot"]),
        answers=AnswersConfig(admin=load_admin_answers(answers_dct["admin"]),
                              user=load_user_answers(answers_dct["user"]))
    )


def load_bot_config(dct: dict) -> BotConfig:
    return BotConfig(
        token=dct["token"],
        superusers=dct["superusers"],
        use_redis=dct['use_redis'],
        channel_username=dct['channel_username'],
        channel_url=dct['channel_url'],
        channel_id=dct['channel_id'],
        openai_api_key=dct['openai_api_key'],
        rate_limit=dct['rate_limit'],
        daily_limit=dct['daily_limit']
    )


def load_user_answers(dct: dict) -> UserAnswer:
    return UserAnswer(
        start=dct["start"],
        reels=dct["reels"],
        hashtag=dct['hashtag'],
        gpt=dct['gpt'],
        not_in_channel=dct['not_in_channel'],
        rate_limit=dct['rate_limit'],
        wait_download=dct['wait_download'],
        wrong_url=dct['wrong_url'],
        daily_limit=dct['daily_limit'],
        hashtag_not_found=dct['hashtag_not_found'],
        hashtags_found=dct['hashtags_found'],
        wait_gpt=dct['wait_gpt'],
        unavailable_gpt=dct['unavailable_gpt'],
        permission_error_gpt=dct['permission_error_gpt'],
        permission_error_gpt_admin=dct['permission_error_gpt_admin'],
        pre_add=dct['pre_add'],
        pre_add_url=dct['pre_add_url'],
        add=dct['add']
       
    )


def load_admin_answers(dct: dict) -> AdminAnswer:
    return AdminAnswer(
        admin=dct["admin"],
        stats_wait=dct["stats_wait"],
        stats=dct['stats'],
        stats_file_wait=dct['stats_file_wait'],
        spam=dct['spam'],
        spam_check=dct['spam_check'],
        spam_beginning=dct['spam_beginning'],
        spam_end=dct['spam_end']
    )
