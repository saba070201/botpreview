import datetime
import logging
from pathlib import Path

import openai
from openai import error

from app.config import load_config
from app.models.config.main import Paths

logger = logging.getLogger(__name__)

config = load_config(Paths(Path().absolute()))
token_list = config.bot.openai_api_key
token_reset_date = datetime.datetime.now()

# openai.api_key = token_list[0]
# models = openai.Model.list()
# print(models)
# for asd in  models['data']:
#     print(asd['id'])
async def ask(text):
    openai.api_key = token_list[0]
    try:
        completion = await openai.ChatCompletion.acreate(model="gpt-3.5-turbo",
                                                         messages=[{"role": "user", "content": text}])
    except error.Timeout as e:
        logger.critical(e.error.message)
        await ask(text)
    except (error.AuthenticationError, error.RateLimitError, error.PermissionError) as e:
        logger.critical(e.error.message)
        if len(token_list) > 1:
            tmp_token = token_list.pop(0)
            logger.warning(f"Removed token {tmp_token} from token list")
            logger.warning(f"New token list : {token_list}")
        else:
            if datetime.datetime.now() - token_reset_date < datetime.timedelta(hours=1):
                raise error.PermissionError
            token_list.extend(config.bot.openai_api_key[:-1])
        await ask(text)
    else:
        return completion.choices[0].message.content
