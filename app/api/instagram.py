from collections import deque
from datetime import datetime
from pathlib import Path
from time import time
from typing import List, Awaitable, Callable, ParamSpec, TypeVar, Iterator
from urllib.parse import urlparse, ParseResult

import instaloader
from instaloader import Hashtag, TopSearchResults, Instaloader

project_path = Path().absolute()
download_path = Path(project_path, 'downloads')
if not download_path.exists():
    download_path.mkdir()

P = ParamSpec("P")
R = TypeVar("R")

insta_sessions = deque()

for path in Path(project_path, 'sessions').iterdir():
    if path.name.startswith('session-'):
        insta_sessions.append(path.name[8:])


def force_async(function: Callable[P, R]) -> Callable[P, Awaitable[R]]:
    '''
    turns a sync function to async function using threads
    :param sync function:
    :return async function:
    '''
    from concurrent.futures import ThreadPoolExecutor
    import asyncio
    pool = ThreadPoolExecutor()

    def wrapper(*args: P.args, **kwargs: P.kwargs) -> Awaitable[R]:
        future = pool.submit(function, *args, **kwargs)
        return asyncio.wrap_future(future)  # make it awaitable

    return wrapper


def get_file_list(item_path: Path) -> List[Path]:
    files = []
    for item in item_path.iterdir():
        if item.suffix not in ('.xz',):
            if item.suffix == '.jpg':
                if Path(item_path, f'{item.stem}.mp4').exists():
                    continue
            files.append(item)
    return files


def format_url(url: str) -> ParseResult:
    parsed_url = urlparse(url)
    if parsed_url.netloc[:4] == 'www.':
        parsed_url = parsed_url._replace(netloc=parsed_url.netloc[4:])
    if parsed_url.netloc != 'instagram.com':
        raise ValueError
    return parsed_url


class PrivateAccountException(Exception):
    pass


def get_instaloader(session_name: str) -> Instaloader:
    loader = instaloader.Instaloader(quiet=True)
    loader.load_session_from_file(username=session_name,
                                  filename=str(Path(project_path, f"sessions/session-{session_name}")))
    loader.filename_pattern = str(datetime.now().timestamp()).replace('.', '')
    return loader


@force_async
def get_highlight_url(url: ParseResult, loader: Instaloader) -> ParseResult:
    response = loader.context._session.get(url.geturl(), params={'Viewport-Width': 1920}, allow_redirects=False)
    while response.status_code in [301, 302]:
        response = loader.context._session.get(response.headers['Location'], params={'Viewport-Width': 1920})
    return format_url(response.url)


@force_async
def get_highlight(url: ParseResult, loader: Instaloader):
    highlight_id = url.path.split('/')[-2]
    params = {'referer': f'https://www.instagram.com/stories/highlights/{highlight_id}/'}
    data = loader.context.get_iphone_json(f'api/v1/feed/reels_media/?reel_ids=highlight:{highlight_id}',
                                          params=params)
    return data['reels'][f'highlight:{highlight_id}']['items']


@force_async
def get_post(url: ParseResult, loader: Instaloader) -> List[Path]:
    short_code = url.path.split('/')[2]
    post = instaloader.Post.from_shortcode(loader.context, short_code)
    item_path = Path(download_path, loader.filename_pattern)
    loader.download_post(post, item_path)
    return get_file_list(item_path)


@force_async
def get_story(url: ParseResult, loader: Instaloader) -> List[Path]:
    username = url.path.split('/')[2]
    print(f"Checking @{username}")
    profile = instaloader.Profile.from_username(loader.context, username)
    if profile.is_private:
        raise PrivateAccountException
    media_id = int(url.path.split('/')[3])
    item = instaloader.StoryItem.from_mediaid(loader.context, media_id)
    item_path = Path(download_path, loader.filename_pattern)
    loader.download_storyitem(item, item_path)
    return get_file_list(item_path)


class NewHashtag(Hashtag):
    @property
    def media_count(self) -> int:
        try:
            return self._node["edge_hashtag_to_media"]["count"]
        except KeyError:
            return self._node["media_count"]

    @property
    def search_result_subtitle(self) -> str | int:
        try:
            count = self._node["search_result_subtitle"].split(" ")[0]
            if not count[0].isnumeric():
                return self.media_count
            return count
        except KeyError:
            return self.media_count


class NewHashtagResults(TopSearchResults):
    def get_hashtags(self) -> Iterator[NewHashtag]:
        for hashtag in self._node.get('hashtags', []):
            node = hashtag.get('hashtag', {})
            if 'name' in node:
                yield NewHashtag(self._context, node)


@force_async
def get_hashtag(hashtag: str, loader: Instaloader) -> List[NewHashtag]:
    search = NewHashtagResults(loader.context, f'#{hashtag}')
    return [*search.get_hashtags()]
