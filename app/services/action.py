from datetime import datetime

from app.dao.action import ActionDAO
from app.enums.chat_type import ActionType
from app.models.db import Action


async def count_user_actions(action_dao: ActionDAO,
                             user_id: int) -> int:
    today_start = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    return await action_dao.count_user_actions(user_id=user_id,
                                               today_start=today_start)


async def insert_action(action_dao: ActionDAO,
                        user_id: int,
                        action_type: ActionType):
    action = Action()
    action.type = action_type
    action.user_id = user_id
    action_dao.save(action)
    await action_dao.commit()
