from typing import List

from app.dao import UserDAO
from app.models import dto


async def upsert_user(user: dto.User, user_dao: UserDAO) -> dto.User:
    saved_user = await user_dao.upsert_user(user)
    await user_dao.commit()
    return saved_user


async def increment_function_count(user: dto.User, user_dao: UserDAO):
    await user_dao.increment_function_count(user)
    await user_dao.commit()


async def get_all_users(user_dao: UserDAO) -> List[dto.User]:
    return await user_dao.get_all()


async def set_subscriber(user: dto.User, user_dao: UserDAO, status: bool):
    target_user = await user_dao.get_by_id(user.id)
    target_user.is_subscriber = status
    await user_dao.commit()


async def count_users(user_dao: UserDAO):
    return await user_dao.count()


async def count_subscribers(user_dao: UserDAO):
    return await user_dao.count_subscribers()


async def set_referral_link(user: dto.User, user_dao: UserDAO, referral_link: str):
    await user_dao.set_referral_link(user, referral_link)
    await user_dao.commit()
