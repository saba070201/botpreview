from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton

menu = InlineKeyboardMarkup(inline_keyboard=[
    [InlineKeyboardButton(text="Скачать пост, рилс или сторис",
                          callback_data="reels")],
    [InlineKeyboardButton(text="Скачать хештеги",
                          callback_data="hashtag")],
    [InlineKeyboardButton(text="Спросить у ChatGPT",
                          callback_data="gpt")]

])

back = InlineKeyboardMarkup(inline_keyboard=[
    [InlineKeyboardButton(text="Назад в меню",
                          callback_data="start")
     ]
])

channel_check = InlineKeyboardMarkup(inline_keyboard=[
    [InlineKeyboardButton(text="Назад в меню",
                          callback_data="start")
     ],
    [InlineKeyboardButton(text="Проверить подписку",
                          callback_data="check_channel")
     ]
])

admin_menu = InlineKeyboardMarkup(inline_keyboard=[
    [InlineKeyboardButton(text="Получить статистику",
                          callback_data="stats")],
    [InlineKeyboardButton(text="Сделать рассылку",
                          callback_data="spam")]

])

spam_menu = InlineKeyboardMarkup(inline_keyboard=[
    [InlineKeyboardButton(text="Всё верно",
                          callback_data="spam_accept")],
    [InlineKeyboardButton(text="Редактировать",
                          callback_data="spam")]
])

admin_back = InlineKeyboardMarkup(inline_keyboard=[
    [InlineKeyboardButton(text="Назад в меню",
                          callback_data="admin")]

])

stats_menu = InlineKeyboardMarkup(inline_keyboard=[
    [InlineKeyboardButton(text="Скачать сводку",
                          callback_data="stats_dl")]

])
